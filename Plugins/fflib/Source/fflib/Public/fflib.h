// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "ModuleManager.h"

// Log class declaration in .h
DECLARE_LOG_CATEGORY_EXTERN(fflibLog, Log, All);

// Ridiculously handy macro to log easily
// Example: LOG("Value = %d", value);
// Example: LOG("%s", *f_string);
#define LOG(M,...) {\
    UE_LOG(fflibLog, Warning, L""#M, ##__VA_ARGS__); \
    if(GEngine && (UE_BUILD_DEBUG || UE_BUILD_DEVELOPMENT)) \
        GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Red, FString::Printf(L""#M, ##__VA_ARGS__)); \
}

class FfflibModule : public IModuleInterface
{
public:
    class Player* player{ nullptr };
    static FfflibModule& Get() { return FModuleManager::LoadModuleChecked<FfflibModule>("fflib"); }
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};
