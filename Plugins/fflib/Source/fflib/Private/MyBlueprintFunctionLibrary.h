// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "MyBlueprintFunctionLibrary.generated.h"

UCLASS()
class UMyBlueprintFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
    UFUNCTION(BlueprintCallable, Category = FFLIB)
    static class UTexture2D* GetPlayerTexture(int index);
    UFUNCTION(BlueprintCallable, Category = FFLIB)
    static float GetPlayerWidth();
};
