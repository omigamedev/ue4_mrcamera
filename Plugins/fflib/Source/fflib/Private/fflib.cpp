// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "fflibPrivatePCH.h"
#include "Engine.h"
#include "Core.h"
#include "ModuleManager.h"
#include "IPluginManager.h"
#include "RenderingThread.h"

DEFINE_LOG_CATEGORY(fflibLog);

#define LOCTEXT_NAMESPACE "FfflibModule"

static FString error2str(int err)
{
    //static char str[AV_ERROR_MAX_STRING_SIZE]{0};
    static char str[256]{ 0 };
    if (av_strerror(err, str, sizeof(str)) < 0)
        return TEXT("Unknown error");
    return UTF8_TO_TCHAR(str);
}

class Player : public FRunnable
{
    FRunnableThread* thread = nullptr;
    FThreadSafeBool running = false;
    AVFormatContext* ctx = nullptr;
    int video_stream_id = -1;
    AVStream* stream = nullptr;
    AVCodec* decoder = nullptr;
    AVCodecContext* codec_ctx = nullptr;
    SwsContext* sws_ctx = nullptr;
    uint8_t* data[4];
    int linesize[4];
    FThreadSafeCounter currentTexture;
    class UTexture2D* tex[3]{ nullptr };
public:
    int width = 0;
    int height = 0;
    bool Open(const char* filename)
    {
        //tex[0] = UTexture2D::CreateTransient(512, 512, PF_G8);
        //tex[1] = UTexture2D::CreateTransient(512, 512, PF_G8);
        //tex[2] = UTexture2D::CreateTransient(512, 512, PF_G8);
        //tex[0]->AddToRoot();
        //tex[1]->AddToRoot();
        //tex[2]->AddToRoot();

        av_register_all();
        avformat_network_init();

        int ret;
        if ((ret = avformat_open_input(&ctx, filename, nullptr, nullptr)) < 0)
        {
            LOG("Format::Open - %s\n", *error2str(ret));
            return false;
        }

        if ((ret = avformat_find_stream_info(ctx, nullptr)) < 0)
        {
            LOG("Format::Open - %s\n", *error2str(ret));
            avformat_close_input(&ctx);
            return false;
        }

        // Start reading network stream
        av_read_play(ctx);

        for (int i = 0; i < (int)ctx->nb_streams; i++)
        {
            if (ctx->streams[i]->codec->codec_type == AVMediaType::AVMEDIA_TYPE_VIDEO)
            {
                video_stream_id = i;

                stream = ctx->streams[i];
                codec_ctx = stream->codec;
                width = codec_ctx->width;
                height = codec_ctx->height;

                if ((decoder = avcodec_find_decoder(stream->codec->codec_id)) == nullptr)
                {
                    LOG("IDecoder::Open \"Could not find a decoder\"\n");
                    return false;
                }

                if (avcodec_open2(stream->codec, decoder, nullptr) < 0)
                {
                    //printf("IDecoder::Open \"Could not open the decoder %.32s\"\n", avcodec_get_name(stream->codec->codec_id));
                    auto descr = avcodec_descriptor_get(stream->codec->codec_id);
                    LOG("IDecoder::Open \"Could not open the decoder %.32s\"\n", descr->name);
                    return false;
                }

                sws_ctx = sws_getContext(width, height, codec_ctx->pix_fmt, width, height, AV_PIX_FMT_BGRA, SWS_BILINEAR, NULL, NULL, NULL);
                if (av_image_alloc(data, linesize, width, height, AV_PIX_FMT_BGRA, 16) < 0)
                {
                    LOG("Could not allocate source image\n");
                    return false;
                }

                tex[0] = UTexture2D::CreateTransient(width, height, PF_G8);
                tex[1] = UTexture2D::CreateTransient(width / 2, height / 2, PF_G8);
                tex[2] = UTexture2D::CreateTransient(width / 2, height / 2, PF_G8);

                tex[0]->UpdateResource();
                tex[1]->UpdateResource();
                tex[2]->UpdateResource();

                tex[0]->AddToRoot();
                tex[1]->AddToRoot();
                tex[2]->AddToRoot();

                break;
            }
        }
        thread = FRunnableThread::Create(this, TEXT("PlayerThread"));
        return true;
    }
    virtual uint32 Run() override
    {
        running = true;
        AVPacket pkt;
        av_init_packet(&pkt);
        AVFrame* frame = av_frame_alloc();

        while (running && av_read_frame(ctx, &pkt) >= 0)
        {
            if (pkt.stream_index == video_stream_id)
            {
                int got_frame = 0;
                int len = avcodec_decode_video2(codec_ctx, frame, &got_frame, &pkt);
                if (len < 0) 
                {
                    LOG("Error while decoding frame\n");
                    return 0;
                }
                if (got_frame) 
                {
                    //sws_scale(sws_ctx, frame->data, frame->linesize, 0, height, data, linesize);
                    auto region_cleaner = [](uint8 *SrcData, const FUpdateTextureRegion2D *Regions)
                    {
                        delete[] Regions;
                    };
                    auto data_cleaner = [](uint8 *SrcData, const FUpdateTextureRegion2D *Regions)
                    {
                        delete[] Regions;
                        delete SrcData;
                    };

                    int size_Y = frame->linesize[0]*height;
                    uint8_t* buffer_Y = new uint8_t[size_Y];
                    memcpy(buffer_Y, frame->data[0], size_Y);

                    int size_U = frame->linesize[1] * height / 2;
                    uint8_t* buffer_U = new uint8_t[size_U];
                    memcpy(buffer_U, frame->data[1], size_U);

                    int size_V = frame->linesize[2] * height / 2;
                    uint8_t* buffer_V = new uint8_t[size_V];
                    memcpy(buffer_V, frame->data[2], size_V);

                    Update(frame, buffer_Y, buffer_U, buffer_V);

                    //if (tex[0]->IsValidLowLevel())
                    //    tex[0]->UpdateTextureRegions(0, 1, new FUpdateTextureRegion2D(0, 0, 0, 0, width, height), frame->linesize[0], 1, buffer_Y, data_cleaner);
                    //else delete buffer_Y;
                    //if (tex[1]->IsValidLowLevel())
                    //    tex[1]->UpdateTextureRegions(0, 1, new FUpdateTextureRegion2D(0, 0, 0, 0, width / 2, height / 2), frame->linesize[1], 1, buffer_U, data_cleaner);
                    //else delete buffer_U;
                    //if (tex[2]->IsValidLowLevel())
                    //    tex[2]->UpdateTextureRegions(0, 1, new FUpdateTextureRegion2D(0, 0, 0, 0, width / 2, height / 2), frame->linesize[2], 1, buffer_V, data_cleaner);
                    //else delete buffer_V;
                    //LOG("Got Frame");

                }
            }
            av_packet_unref(&pkt);
            av_init_packet(&pkt);
        }

        av_frame_free(&frame);

        if (!running)
            avformat_close_input(&ctx);
        
        return 0;
    }

    void Update0(AVFrame* frame, uint8_t* buffer_Y, uint8_t* buffer_U, uint8_t* buffer_V)
    {
        struct FUpdateTextureRegionsData
        {
            FTexture2DResource* Texture2DResource;
            int32 MipIndex;
            uint32 NumRegions;
            const FUpdateTextureRegion2D* Regions;
            uint32 SrcPitch;
            uint32 SrcBpp;
            uint8* SrcData;
        };

        FUpdateTextureRegionsData* RegionData = new FUpdateTextureRegionsData;

        RegionData->Texture2DResource = (FTexture2DResource*)tex[0]->Resource;
        RegionData->MipIndex = 0;
        RegionData->NumRegions = 1;
        RegionData->Regions = new FUpdateTextureRegion2D(0, 0, 0, 0, width, height);
        RegionData->SrcPitch = frame->linesize[0];
        RegionData->SrcBpp = 1;
        RegionData->SrcData = buffer_Y;

        auto DataCleanupFunc = [](uint8 *SrcData, const FUpdateTextureRegion2D *Regions)
        {
            delete[] Regions;
            delete SrcData;
        };

        ENQUEUE_UNIQUE_RENDER_COMMAND_TWOPARAMETER(
            UpdateTextureRegionsData,
            FUpdateTextureRegionsData*, RegionData, RegionData,
            TFunction<void(uint8* SrcData, const FUpdateTextureRegion2D* Regions)>, DataCleanupFunc, DataCleanupFunc,
            {
                for (uint32 RegionIndex = 0; RegionIndex < RegionData->NumRegions; ++RegionIndex)
                {
                    int32 CurrentFirstMip = RegionData->Texture2DResource->GetCurrentFirstMip();
                    if (RegionData->MipIndex >= CurrentFirstMip)
                    {
                        RHIUpdateTexture2D(
                            RegionData->Texture2DResource->GetTexture2DRHI(),
                            RegionData->MipIndex - CurrentFirstMip,
                            RegionData->Regions[RegionIndex],
                            RegionData->SrcPitch,
                            RegionData->SrcData
                            + RegionData->Regions[RegionIndex].SrcY * RegionData->SrcPitch
                            + RegionData->Regions[RegionIndex].SrcX * RegionData->SrcBpp
                            );
                    }
                }
                DataCleanupFunc(RegionData->SrcData, RegionData->Regions);
                delete RegionData;
            });

    }

    void Update(AVFrame* frame, uint8_t* buffer_Y, uint8_t* buffer_U, uint8_t* buffer_V)
    {
        struct FUpdateTextureRegionsData
        {
            FTexture2DResource* resource;
            uint8_t* data;
            int width;
            int height;
            int linesize;
        };

        FUpdateTextureRegionsData* RegionData = new FUpdateTextureRegionsData[3];
        RegionData[0] = { (FTexture2DResource*)tex[0]->Resource, buffer_Y, width, height, frame->linesize[0] };
        RegionData[1] = { (FTexture2DResource*)tex[1]->Resource, buffer_U, width / 2, height / 2, frame->linesize[1] };
        RegionData[2] = { (FTexture2DResource*)tex[2]->Resource, buffer_V, width / 2, height / 2, frame->linesize[2] };
        auto DataCleanupFunc = [](uint8 *SrcData, const FUpdateTextureRegion2D *Regions)
        {
            delete[] Regions;
            delete SrcData;
        };

        if (tex[0]->IsValidLowLevel() && tex[1]->IsValidLowLevel() && tex[2]->IsValidLowLevel())
        {
            ENQUEUE_UNIQUE_RENDER_COMMAND_TWOPARAMETER(
                UpdateTextureRegionsDataYUV,
                FUpdateTextureRegionsData*, RegionData, RegionData,
                TFunction<void(uint8* SrcData, const FUpdateTextureRegion2D* Regions)>, DataCleanupFunc, DataCleanupFunc,
                {
                    for (int i = 0; i < 3; i++)
                    {
                        if (RegionData[i].resource)
                        {
                            int32 CurrentFirstMip = RegionData[i].resource->GetCurrentFirstMip();
                            if (0 >= CurrentFirstMip)
                            {
                                RHIUpdateTexture2D(
                                    RegionData[i].resource->GetTexture2DRHI(),
                                    0 - CurrentFirstMip,
                                    FUpdateTextureRegion2D(0, 0, 0, 0, RegionData[i].width, RegionData[i].height),
                                    RegionData[i].linesize,
                                    RegionData[i].data
                                    );
                            }
                        }
                        delete RegionData[i].data;
                    }
                    delete[] RegionData;
                });
        }
        else
        {
            delete buffer_Y;
            delete buffer_U;
            delete buffer_V;
        }
    }

    virtual void Stop() override
    {
        running = false;
        //thread->WaitForCompletion();
    }

    UTexture2D* GetTexture(int index)
    {
        return tex[index];
    }

};

void FfflibModule::StartupModule()
{
    // Retrieve config
    FString URL;
    if (GConfig->GetString(TEXT("MixedReality"), TEXT("CameraURL"), URL, GGameIni))
    {
        player = new Player;
        player->Open(TCHAR_TO_ANSI(*URL));
        FCoreDelegates::OnPreExit.AddLambda([&] { player->Stop(); });
    }
}

void FfflibModule::ShutdownModule()
{
    if (player)
        player->Stop();
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FfflibModule, fflib)