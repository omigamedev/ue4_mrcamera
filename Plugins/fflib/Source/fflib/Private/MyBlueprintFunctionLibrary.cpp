// Fill out your copyright notice in the Description page of Project Settings.

#include "fflibPrivatePCH.h"
#include "MyBlueprintFunctionLibrary.h"
#include "fflib.h"

class UTexture2D* UMyBlueprintFunctionLibrary::GetPlayerTexture(int index)
{
    return FfflibModule::Get().player->GetTexture(index);
}

float UMyBlueprintFunctionLibrary::GetPlayerWidth()
{
    return FfflibModule::Get().player->width;
}
