// Fill out your copyright notice in the Description page of Project Settings.

using System.IO;
using UnrealBuildTool;

public class ffmpeg : ModuleRules
{
	public ffmpeg(ReadOnlyTargetRules Target) : base(Target)
    {
		Type = ModuleType.External;

		if (Target.Platform == UnrealTargetPlatform.Win64)
		{
			// Add the import library
			PublicLibraryPaths.Add(Path.Combine(ModuleDirectory, "64", "lib"));
			PublicIncludePaths.Add(Path.Combine(ModuleDirectory, "64", "include"));
            PublicAdditionalLibraries.Add("libavcodec.a");
            PublicAdditionalLibraries.Add("libavdevice.a");
            PublicAdditionalLibraries.Add("libavformat.a");
            PublicAdditionalLibraries.Add("libavutil.a");
            PublicAdditionalLibraries.Add("libswscale.a");
            PublicAdditionalLibraries.Add("libswresample.a");
            PublicAdditionalLibraries.Add("shlwapi.lib");
            PublicAdditionalLibraries.Add("ws2_32.lib");
            PublicAdditionalLibraries.Add("secur32.lib");
		}
	}
}
