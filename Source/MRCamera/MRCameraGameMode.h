// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "MRCameraGameMode.generated.h"

UCLASS()
class MRCAMERA_API AMRCameraGameMode : public AGameMode
{
    GENERATED_BODY()
public:
    TSharedPtr<SWindow> Window;
    FSlateBrush Brush;
    virtual void BeginPlay() override;
    virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
};
