// Fill out your copyright notice in the Description page of Project Settings.

#include "MRCamera.h"
#include "MRCameraGameMode.h"

#pragma comment(lib, "dxgi.lib")
#include "AllowWindowsPlatformTypes.h"
#include <dxgi1_3.h>
#include "HideWindowsPlatformTypes.h"
struct Output
{
    IDXGIOutput* dx_output{ nullptr };
    IDXGIOutputDuplication* dx_desktop{ nullptr };
    char name[128]{ 0 };
    bool attached{ false };
    RECT rect{ 0 };

    Output(IDXGIOutput* output);
    Output(const Output& other) = delete;
    Output(Output&& other);
    ~Output();

    bool operator==(const Output& other) const;
};

struct Adapter
{
    IDXGIAdapter* dx_adapter;
    LUID luid;
    int memory;
    char description[128];

    Adapter(IDXGIAdapter* adapter);
    Adapter(const Adapter& other) = delete;
    Adapter(Adapter&& other);
    ~Adapter();

    bool operator==(const Adapter& other) const;
    bool operator==(const LUID& otherLuid) const;

    TArray<Output> GetOutputs() const;
};


bool Adapter::operator==(const LUID& otherLuid) const
{
    return !((otherLuid.HighPart ^ luid.HighPart) | (otherLuid.LowPart ^ luid.LowPart));
}

bool Adapter::operator==(const Adapter& other) const
{
    return !((other.luid.HighPart ^ luid.HighPart) | (other.luid.LowPart ^ luid.LowPart));
}

Adapter::Adapter(Adapter&& other)
{
    dx_adapter = other.dx_adapter;
    other.dx_adapter = nullptr;
    memory = other.memory;
    other.memory = 0;
    strcpy_s(description, other.description);
    luid = other.luid;
}

Adapter::Adapter(IDXGIAdapter* adapter)
{
    this->dx_adapter = adapter;

    if (adapter != nullptr)
    {
        DXGI_ADAPTER_DESC desc;
        adapter->GetDesc(&desc);
        memory = desc.DedicatedVideoMemory / 1024u / 1024u;
        wcstombs_s(nullptr, description, sizeof(description), desc.Description, sizeof(desc.Description));
        luid = desc.AdapterLuid;
    }
}

Adapter::~Adapter()
{
    if (dx_adapter)
        dx_adapter->Release();
}

TArray<Output> Adapter::GetOutputs() const
{
    TArray<Output> list;
    IDXGIOutput* pOutput{ nullptr };
    unsigned int outputIndex{ 0 };

    while (dx_adapter->EnumOutputs(outputIndex, &pOutput) != DXGI_ERROR_NOT_FOUND)
    {
        list.Add(pOutput);
        outputIndex++;
    }

    return MoveTemp(list);
}

Output::Output(IDXGIOutput* output)
{
    dx_output = output;
    if (output != nullptr)
    {
        DXGI_OUTPUT_DESC desc;
        output->GetDesc(&desc);
        wcstombs_s(nullptr, name, sizeof(name), desc.DeviceName, sizeof(desc.DeviceName));
        attached = desc.AttachedToDesktop == 1;
        rect = desc.DesktopCoordinates;
    }
}

Output::~Output()
{
    if (dx_output)
        dx_output->Release();
    if (dx_desktop)
        dx_desktop->Release();
}

bool Output::operator==(const Output& other) const
{
    return strcmp(name, other.name) == 0;
}

Output::Output(Output&& other)
{
    dx_output = other.dx_output;
    dx_desktop = other.dx_desktop;
    other.dx_output = nullptr;
    other.dx_desktop = nullptr;
    attached = other.attached;
    strcpy_s(name, other.name);
    other.name[0] = 0;
    rect = other.rect;
}

TArray<Adapter> EnumerateAdapters()
{
    TArray<Adapter> list;
    IDXGIFactory* pFactory{ nullptr };
    IDXGIAdapter* pAdapter{ nullptr };
    unsigned int adapterIndex{ 0 };

    CreateDXGIFactory1(__uuidof(IDXGIFactory), reinterpret_cast<void**>(&pFactory));
    while (pFactory->EnumAdapters(adapterIndex, &pAdapter) != DXGI_ERROR_NOT_FOUND)
    {
        list.Add(pAdapter);
        adapterIndex++;
    }
    pFactory->Release();

    return MoveTemp(list);
}

void AMRCameraGameMode::BeginPlay()
{
    UMaterial* Capture = LoadObject<UMaterial>(this, TEXT("Material'/Game/M_BrushMaterial.M_BrushMaterial'"));
    Brush.SetResourceObject(Capture);
    Brush.DrawAs = ESlateBrushDrawType::Image;
    Brush.ImageSize = FVector2D(256, 256);

    Window = SNew(SWindow)
        .AutoCenter(EAutoCenter::None)
        .ScreenPosition(FVector2D(100, 100))
        .ClientSize(FVector2D(1024, 768))
        .UseOSWindowBorder(true)
        .CreateTitleBar(true)
        .ActivateWhenFirstShown(true)
        .Title(FText::FromString(TEXT("Secondary Window")))
        [
            SNew(SImage).Image(&Brush)
        ];
    FSlateApplication::Get().AddWindow(Window.ToSharedRef());
    
    TSharedPtr<SWindow> VRWindow;
    TArray<TSharedRef<SWindow>> WindowsList;
    FSlateApplication::Get().GetAllVisibleWindowsOrdered(WindowsList);
    for (auto w : WindowsList)
    {
        if (w->HasOSWindowBorder()) // This should be the VR preview
        {
            VRWindow = w;
        }
    }

    auto filter = [](const Adapter& a, const Adapter& b) { return a.memory < b.memory; };
    auto adapters = EnumerateAdapters();
    adapters.Sort(filter);
    auto& selected = adapters.Last();

    for (const auto& a : adapters)
    {
        LOG("adapter %s memory %d", ANSI_TO_TCHAR(a.description), a.memory);
        auto monitors = a.GetOutputs();
        for (const auto& m : monitors)
        {
            LOG("-- monitor %s (%d, %d, %d, %d)", ANSI_TO_TCHAR(m.name), m.rect.left, m.rect.top, m.rect.right, m.rect.bottom);
        }
    }

    auto monitors = selected.GetOutputs();
    if (monitors.Num() > 1)
    {
        const auto& m = monitors[1];
        Window->MoveWindowTo(FVector2D( m.rect.left, m.rect.top ));
        Window->Resize(FVector2D( m.rect.right - m.rect.left, m.rect.bottom - m.rect.top ));
    }
    else
    {
        VRWindow->MoveWindowTo({ 0, 20 });
        VRWindow->Resize({ 960, 1080 });
        Window->MoveWindowTo({ 960, 20 });
        Window->Resize({ 960, 1080 });
    }
}

void AMRCameraGameMode::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
    Window->RequestDestroyWindow();
}
