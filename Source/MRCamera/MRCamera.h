// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine.h"

// Log class declaration in .h
DECLARE_LOG_CATEGORY_EXTERN(MRCamera, Log, All);

// Ridiculously handy macro to log easily
// Example: LOG("Value = %d", value);
// Example: LOG("%s", *f_string);
#define LOG(M,...) {\
    UE_LOG(MRCamera, Warning, L""#M, ##__VA_ARGS__); \
    if(GEngine && (UE_BUILD_DEBUG || UE_BUILD_DEVELOPMENT)) \
        GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Red, FString::Printf(L""#M, ##__VA_ARGS__)); \
}
